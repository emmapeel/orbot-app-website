dir: "ltr"
site:
  title: 'Orbot - Tor per als mòbils'
bottom_contact_area:
  title: "Contacteu."
  content: "Voleu donar suport econòmicament al projecte d'altra manera? Contacteu."
  href: "https://guardianproject.info/ca/contact/"
  action: "Contacta"
bottom_donate_area:
  title: "Lliure i obert."
  content: "L'Orbot és una aplicació lliure! Doneu suport al desenvolupament continu
    de l'aplicació. Compartiu l'amor."
  action: "Fes una donació"
footer:
  copyright: "Drets d'autor © 2009-%Y Guardian Project. Tots els drets reservats."
main_navigation:
  donate: "Fes una donació"
  responsive_menu_links:
  - url: "/"
    title: "Inici"
  - url: "/about/"
    title: "Quant a"
  - url: "/privacy-policy/"
    title: "Privadesa"
  - url: "/legal/"
    title: "Legal"
  - url: "/download/"
    title: "Baixada"
  - url: "/faqs/"
    title: "PMF"
  - url: "/donate/"
    title: "Donació"
locale_selector:
  title: "Llengua"
about:
  special_area:
    title: 'Orbot - VPN Tor per a mòbils'
    subtitle: 'Manteniu el trànsit de les aplicacions privat i desblocat.'
  title: 'Orbot - Manteniu les aplicacions segures'
  description: 'L''Orbot és una VPN Tor per a l''Android. Manté les aplicacions segures!'
  tor_area:
    image: '/assets/img/Tor.png'
    description: 'El Tor s''impulsa per una xarxa diversa de persones. Quan utilitzeu
      el Tor, cap empresa ni persona té accés a la vostra activitat de navegació.'
    text: 'Més informació sobre el Tor a'
    url: 'https://torproject.org/ca/'
    tor_items:
    - image: '/assets/img/EncryptedTraffic.png'
      alt: 'EncryptedTraffic-icon'
      title: 'Trànsit xifrat.'
      text: 'El vostre trànsit d''internet és xifrat i rebotat a tres parts del món
        diferents abans d''arribar a la seva destinació.'
    - image: '/assets/img/OnlinePrivacy.png'
      alt: 'OnlinePrivacy-icon'
      title: 'Privadesa en línia.'
      text: 'Les terceres parts no sabran quines aplicacions utilitzeu. La vostra
        adreça IP real no es revela.'
    - image: '/assets/img/NoCensorship.png'
      alt: 'NoCensorship-icon'
      title: 'Sense censura.'
      text: 'Accediu a aplicacions i contingut independentment de la vostra ubicació
        o restriccions de la xarxa.'
    - image: '/assets/img/NoSurveillance.png'
      alt: 'NoSurveillance-icon'
      title: 'Sense vigilància.'
      text: 'Els proveïdors de servei d''internet (ISP) i els operadors de la xarxa
        Wi-Fi no poden veure la vostra navegació.'
    - image: '/assets/img/NoTracking.png'
      alt: 'NoTracking-icon'
      title: 'Sense rastreig.'
      text: 'Sou en control. Impediu que terceres parts us rastregin.'
  connected_area:
    image: '/assets/img/AboutScreens.png'
    link: 'https://www.csoonline.com/article/3287653/what-is-the-tor-browser-how-it-works-and-how-it-can-help-you-protect-your-identity-online.html'
  disclaimer_area:
    disclaimer_items:
    - title: 'Avís legal'
      text_nodes:
      - node: 'Fer servir l''Orbot no garanteix la seguretat i privadesa per si mateix.
          Només proporciona una sèrie de característiques que podrien millorar la
          privadesa i anonimat del vostre trànsit de la xarxa. Recordeu el següent…'
      - node: 'Si feu servir l''Orbot per a iniciar sessió en aplicacions a què normalment
          accediu fora de la xarxa Tor, l''aplicació podria ser capaç d''identificar-vos
          i saber que esteu utilitzant Tor. En algunes circumstàncies (p. ex. dissidents
          polítics en nacions repressives), això podria ser considerat informació
          incriminatòria.'
    - title: 'Geolocalització, fitxers multimèdia i informació mòbil'
      text_nodes:
      - node: 'Les aplicacions podrien fer ús de l''API de geolocalització (per a
          veure la vostra ubicació GPS actual), accedir a les fotos i vídeos en l''emmagatzematge
          extern, o extraure dades sobre el telèfon mòbil, com ara el número de telèfon
          o la informació de la companyia telefònica. Els usuaris també haurien de
          vigilar qualsevol finestra emergent que demani permisos d''accés.'
    - title: 'Més informació'
      text_nodes:
      - node: 'El Tor Project <a href="https://support.torproject.org/faq/staying-anonymous/">manté
          una pàgina de consells</a> sobre com romandre anònim mentre s''utilitza
          el Tor.'
      - node: 'Un registre de les <a href="https://github.com/guardianproject/orbot/releases">notes
          de les versions</a> de l''Orbot, incloent-hi errors de seguretat previs,
          correccions d''errors i més <a href="https://github.com/guardianproject/orbot/releases">es
          troba disponible aquí</a>. També podeu anar a la pàgina de GitHub del projecte
          per a veure <a href="https://github.com/guardianproject/orbot/issues">una
          llista dels errors coneguts actualment.</a>'
code:
  title: 'Orbot | Codi'
  description: 'Codi font obert de l''Orbot'

  title_section: 'Codi'
  code_items:
  - question: 'Orbot per a l''Android'
    answer: 'El codi principal es troba disponible al [GitHub]((https://github.com/guardianproject/orbot/)
      i al [Gitlab](https://gitlab.com/guardianproject/orbot/)'
  - question: 'Orbot per a l''iOS'
    answer: 'El codi principal es troba disponible al [GitHub](https://github.com/guardianproject/orbot-ios)'
  - question: 'Tor-Android'
    answer: 'L''Orbot conté al nucli el [Tor-Android](https://gitlab.com/guardianproject/tor-android/),
      que permet que qualsevol aplicació ofereixi i gestioni el Tor a dins seu'
  - question: 'Tor.framework per a l''iOS'
    answer: 'L''Orbot per a l''iOS conté al nucli el projecte [Tor.framework](https://github.com/iCepa/Tor.framework/tree/pure_pod),
      que es troba disponible com a un CocoaPad perquè qualsevol aplicació l''integri'
  - question: 'IPtProxy'
    answer: 'L''Orbot també és compatible amb el «Tor Pluggable Transports», com ara
      l''Obfs4 i l''[Snowflake](https://snowflake.torproject.org/) utilitzant la [biblioteca
      IPtProxy](https://github.com/tladesignz/IPtProxy)'
donate:
  title: 'Doneu suport a l''Orbot'
  description: 'L''Orbot el desenvolupa el Guardian Project, una organització fundada
    quasi per complet per beques i donacions d''organitzacions i individuals.'
  subtitle: 'Maneres de donar'
  donate_form:
  - text: 'Envieu 10,00 USD'
    value: '10'
  - text: 'Envieu 20,00 USD'
    value: '20'
  - text: 'Envieu 30,00 USD'
    value: '30'
  - text: 'Envieu 40,00 USD'
    value: '40'
  - text: 'Envieu 50,00 USD'
    value: '50'
  donate_partners:
  - name: 'PayPal'
    url: 'https://paypal.me/guardianproject'
    image: '/assets/img/pp.jpg'
    alt: 'PayPal-logo'
    text: 'Envieu una donació puntual a'
  - name: 'BitCoin'
    url: '198fHnKSkXboZLWqVi2KTRdNrPbdh34Ya5'
    image: '/assets/img/Bitcoin.png'
    alt: 'Bitcoin-logo'
  - name: 'Ether'
    url: '133170647542866872072615948397500080356915441689'
    image: '/assets/img/Ether.png'
    alt: 'Ether-logo'
  - name: 'Patreon'
    url: 'https://patreon.com/GuardianProject'
    image: '/assets/img/Patreon.png'
    alt: 'Patreon-logo'
    text: 'Feu-nos costat al'
  - name: 'Liberapay'
    url: 'https://liberapay.com/GuardianProject'
    image: '/assets/img/liberapay.svg'
    alt: 'Liberapay-img'
    text: 'Feu una donació al'
  - name: 'Patrocinador al GitHub'
    url: 'https://github.com/sponsors/eighthave'
    image: '/assets/img/github.png'
    alt: 'github-logo'
    text: 'Patrocineu-nos al'
download:
  special_area:
    title: 'Orbot - VPN Tor per als mòbils'
    subtitle: 'Baixeu l''Orbot!'
  tor_area:
    text: 'Baixeu altre programari del Tor Project a'
    url: 'https://www.torproject.org/ca/download/'
    tor_items:
    - title: 'Google Play'
      info: 'Podeu instal·lar l''aplicació des del Google Play a [https://play.google.com/store/apps/details?id=org.torproject.android](https://play.google.com/store/apps/details?id=org.torproject.android)'
    - title: 'F-Droid'
      info: 'Podeu aprendre com afegir el repositori d''aplicacions del Guardian Projects
        al F-Droid a[https://guardianproject.info/fdroid](https://guardianproject.info/fdroid)'
    - title: 'App Store d''Apple'
      info: 'Podeu instal·lar la versió oficial de l''Orbot per a l''iOS a [https://apps.apple.com/us/app/orbot/id1609461599](https://apps.apple.com/us/app/orbot/id1609461599).
        També podeu unir-vos al grup de comentaris inicials del Testflight via [https://testflight.apple.com/join/adSqbCeM](https://testflight.apple.com/join/adSqbCeM)'
    - title: 'Baixada directa'
      info: 'Trobareu versions etiquetades amb fitxers descarregables per a fer proves
        al Github a [https://github.com/guardianproject/orbot/releases](https://github.com/guardianproject/orbot/releases)
        i [https://github.com/guardianproject/orbot-ios/releases](https://github.com/guardianproject/orbot-ios/releases)'
    - title: 'Lloc web del Guardian Project'
      info: 'Podeu baixar el fitxer d''aplicació d''Android (APK) de l''Orbot directament
        a [https://guardianproject.info/releases/orbot-latest.apk](https://guardianproject.info/releases/orbot-latest.apk)'
faqs:
  title: 'Orbot - Preguntes Més Freqüents'
  description: 'Pàgina de les PMF de l''Orbot'
  title_section: 'PMF'
  faq_items:
  - question: 'On puc trobar informació sobre l''última versió, errors i millores?'
    answer: 'Publiquem totes les versions i les notes de les versions a les pàgines
      dels nostres projectes de codi obert a [https://github.com/guardianproject/orbot/releases](https://github.com/guardianproject/orbot/releases)
      i [https://github.com/guardianproject/orbot-ios/releases](https://github.com/guardianproject/orbot-ios/releases)'
  - question: 'Què és el Tor?'
    answer: 'El Tor és una xarxa de repetidors administrada per voluntaris d''arreu
      del món, que us permet rebotar-hi les vostres comunicacions per a ocultar els
      llocs web que visiteu a les persones que observen la vostra connexió a internet.
      També evita que els llocs web que visiteu coneguin la vostra ubicació física.'
  - question: 'Per què va més lent l''internet quan estic connectat al Tor?'
    answer: 'Perquè esteu rebotant el vostre trànsit a través dels repetidors administrats
      per voluntaris d''arreu del món, i la vostra connexió es veu afectada pels colls
      d''ampolla i la latència de la xarxa.'
  - question: 'Com sé si estic connectat al Tor?'
    answer: 'En obrir l''Orbot, premeu el botó gran, i després d''uns segons en què
      s''estableix la connexió al Tor veureu un missatge que diu 100% connectat, i
      el botó es tornarà verd. Si esteu fent servir la VPN per a encaminar el trànsit
      d''un navegador a través del Tor, també podeu comprovar la connexió si aneu
      a [https://check.torproject.org/](https://check.torproject.org/), un enllaç
      creat per l''equip del Tor perquè comproveu si esteu connectat al Tor o no.'
  - question: 'Què són els ponts?'
    answer: 'Els ponts són repetidors Tor que us ajuden a circumval·lar la censura.
      Podeu provar els ponts si el vostre proveïdor d''internet bloca el Tor.'
  - question: Per què Orbot iOS es va tornar tan poc fiable?
    answer: Els atacs a la xarxa Tor van augmentar des de la guerra a Ucraïna. Les
      vulnerabilitats de seguretat s'han arreglat des de llavors i s'han afegit nodes,
      però això torna a consumir més memòria. El client Tor és sensible als canvis
      a la xarxa, ja que vol descobrir-ho tot. Com més nodes trobi, més memòria consumeix.
      Malauradament, Apple només permet l'ús de 50 MB de RAM (mega byte, en dispositius
      que tinguin almenys 3 GB - GIGA byte!) en les anomenades "Extensions de xarxa"
      (l'API que s'ha d'utilitzar per a aplicacions d'estil "VPN"). Aquest és un límit
      molt dur per a un programari com Tor. A més, el Tor original que s'utilitza
      actualment escrit en C està en marxa, mentre que una nova implementació Tor,
      escrita en Rust està en marxa, però encara no on ha d'estar. Si us plau sigueu
      pacient.
  - answer: "Vegeu la resposta anterior: a causa de la mida de la porció de la xarxa
      Tor que esteu veient, podeu arribar al límit de 50 MB. iOS elimina l'\"Extensió
      de xarxa\" en aquest cas. Si heu seleccionat \"reinicia en cas d'error\" (activat
      per defecte), intentarà reiniciar-se automàticament."
    question: Per què Orbot iOS es torna a connectar constantment?
  - question: Com puc ajustar Orbot iOS perquè funcioni?
    answer: Prova d'esborrar la memòria cau. De vegades, desfer-se de la informació
      antiga pot alliberar prou memòria. Tanmateix, descobrir cada node de nou fa
      servir *més* memòria que carregar-lo des de la memòria cau. Per tant, si no
      comença la primera vegada, doneu-li més oportunitats de reiniciar-se. Es carregarà
      més i més informació actual de la memòria cau que deixa més memòria per a les
      operacions normals.
  - answer: Aneu a Configuració, activeu "Esborra sempre la memòria cau abans d'iniciar".
      Trigarà més temps, però es reiniciarà, sempre que la part de la xarxa que esteu
      veient no sigui massa gran amb un nou inici.
    question: Comença bé amb una memòria cau nova, però després d'una estona torna
      a fallar.
  - question: Tor es queixa al registre que "MaxMemInQueues" és massa baix i no pot
      construir circuits!
    answer: Hem establert un valor baix (a 5 MB per defecte), de manera que no arribem
      al sostre de 50 MB massa ràpid. Podeu experimentar amb la configuració més alta.
      Aneu a Configuració. Introduïu `--MaxMemInQueues` (dos menys!) seguits a la
      secció «Configuració avançada de Tor», introduïu `10 MB` a la fila següent.
      Reinicieu. Si acabeu en un bucle de reinici, esteu utilitzant massa memòria.
      En aquest cas, traieu aquestes línies de nou.
  - question: Res ajuda, Tor no arrenca.
    answer: Proveu d'utilitzar ponts personalitzats, fins i tot si no els necessiteu
      per evitar el bloqueig. La porció de la xarxa Tor que veieu a través dels ponts
      pot ser més petita, de manera que el client Tor no consumirà tanta memòria.
  - answer: "Premeu «Pregunta a Tor»: actualitzarà la llista de ponts Obfs4 integrada,
      actualitzarà la configuració de Snowflake i també us proporcionarà un munt de
      ponts personalitzats. Torneu a provar totes les combinacions. També podeu utilitzar
      el Telegram o el bot de correu electrònic, us proporcionaran diferents ponts
      d'altres dispositius."
    question: Sembla que els ponts estan bloquejats!
  - answer: Apple va introduir un nou renderitzador web (més ràpid) anomenat `WKWebView`,
      que va substituir `UIWebView` i volia que totes les aplicacions s'adaptessin
      a aquest canvi. Tanmateix, `WKWebView` no admetia el trànsit de servidor intermediari
      com ho feia `UIWebView`. A més, això sempre va ser només una solució temporal,
      ja que mai no podríem representar els fluxos d'àudio/vídeo ni filtrar la vostra
      adreça IP mitjançant WebRTC. Amb Orbot, aquests problemes van desaparèixer.
      Malauradament, Orbot iOS ara va acabar accidentalment en aquest racó, d'on és
      difícil sortir-ne. Des d'iOS 17, `WKWebView` admet, però, el servidor intermediari,
      de manera que ara teniu una altra opció. Si us plau, actualitzeu a iOS 17, si
      podeu!
    question: Per què la nova versió del Navegador Onion es basa en Orbot iOS?
home:
  title: 'Orbot - Manteniu les aplicacions segures.'
  description: 'L''Orbot és una VPN Tor per a l''Android. Manté les aplicacions segures!'
  locale: 'Català'
  banner_area:
    subtitle: 'Orbot - VPN Tor per als mòbils'
    title: 'Manteniu les aplicacions segures'
    app_store_url: 'https://play.google.com/store/apps/details?id=org.torproject.android'
    app_store_button: '/assets/img/en-play-badge.png'
    apple_url: 'https://apps.apple.com/us/app/orbot/id1609461599'
    apple_button: '/assets/img/apple.png'
    fdroid_url: 'https://guardianproject.info/fdroid'
    fdroid_button: '/assets/img/fdroid.png'
    image: '/assets/img/LandingPage.png'
  promo_area:
    promo_area_items:
    - title: 'Privadesa del trànsit'
      text: 'El trànsit xifrat en qualsevol aplicació a través de la xarxa Tor us
        dona el més alt estàndard de seguretat i privadesa.'
      image: '/assets/img/Browsing.png'
      alt: 'Browsing-icon'
    - title: 'Atureu el fureteig'
      text: 'No hi haurà ulls extra que coneguin quines aplicacions utilitzeu, ni
        quan, ni podran impedir que les feu servir.'
      image: '/assets/img/NoExtras.png'
      alt: 'NoExtras-icon'
    - title: 'Historial protegit'
      text: 'Ni l''operador de la xarxa ni els servidors centrals de les aplicacions
        faran cap registre de l''historial de trànsit o de les adreces IP.'
      image: '/assets/img/TossHistory.png'
      alt: 'TossHistory-icon'
  power_area:
    title: 'Funciona amb el Tor'
    text: 'L''Orbot és la vostra connexió de confiança amb el Tor a l''Android i a
      l''iOS.'
    tor_image: '/assets/img/Tor.png'
    link_text: 'Més informació >'
    link_url: 'about'
  detail_area:
    detail_area_items:
    - title: 'Protegeix i desbloca<br/>aplicacions específiques.'
      text: 'L''Orbot (a l''Android) us permet triar específicament quines aplicacions
        encamineu a través del Tor, i això us permet continuar fent servir aplicacions
        i serveis que podrien detectar el trànsit que prové del Tor.'
      image: '/assets/img/AppChoice.jpg'
      alt: 'AppChoice-image'
    - title: 'Accés superràpid i supersegur a serveis onion populars.'
      text: 'Els <a href="https://support.torproject.org/onionservices/" target="_blank">serveis
        Onion</a> són versions dels llocs web que només es poden accedir pel Tor.
        L''Orbot permet que qualsevol aplicació es connecti a un servei onion, i que
        el vostre telèfon n''hostatgi un!'
      image: '/assets/img/OnionSites.jpg'
      alt: 'OnionSites-mage'
    - title: 'Romaneu connectat'
      text: 'L''Orbot ofereix accés a una gran varietat de Ponts Tor, perquè pugueu
        romandre connectat inclús en les xarxes més restringides'
      image: '/assets/img/Bridges.jpg'
      alt: 'Bridges-image'
  featured_area:
    title: 'Destacat en'
    featured_area_items:
    - image: '/assets/img/NewYorkTimes.png'
      alt: 'NewYorkTimes-logo'
      url: "http://www.nytimes.com/2014/02/20/technology/personaltech/privacy-please-tools-to-shield-your-smartphone-from-snoopers.html"
    - image: '/assets/img/ArsTechnica.jpg'
      alt: 'ArsTechnica-logo'
      url: "https://arstechnica.com/information-technology/2012/02/from-encryption-to-darknets-as-governments-snoop-activists-fight-back/"
    - image: '/assets/img/TechCrunch.png'
      alt: 'TechCrunch-logo'
      url: "https://techcrunch.com/2016/01/20/facebook-expands-tor-support-to-android-orbot-proxy/"
    - image: '/assets/img/BoingBoing.png'
      alt: 'BoingBoing-logo'
      url: "https://boingboing.net/2018/10/23/hardware-orbot.html"
    - image: '/assets/img/LifeHacker.png'
      alt: 'LifeHacker-logo'
      url: "https://lifehacker.com/the-apps-that-protect-you-against-verizons-mobile-track-1679936224"
    - image: '/assets/img/Gizmodo.png'
      alt: 'Gizmodo-logo'
      url: "https://gizmodo.com/6-apps-to-secure-your-smartphone-better-1791777911"
  special_area:
    title: '«L''Orbot és la manera més segura de romandre connectat a les aplicacions
      que necessito!»'
    text: 'Ressenya de la Play Store - 21 de maig de 2018'
  dev_area:
    title: 'Quant als desenvolupadors.'
    text: 'L''Orbot és <a href="https://github.com/guardianproject/orbot/">lliure
      i de codi obert</a>. Qualsevol pot ajudar a contribuir, auditar o inspeccionar
      el codi. Els programadors principals inclouen <a href="https://github.com/n8fr8">Nathan
      Freitas</a>, <a href="https://gitlab.com/eighthave">Hans-Christoph Steiner</a>,
      <a href="https://github.com/bitmold">Bim</a>, <a href="https://die.netzarchitekten.com/">Benjamin
      Erhart</a> i més del <a href="https://guardianproject.info/">Guardian Project</a>.'
    github_url: 'https://github.com/guardianproject/orbot'
    github_label: 'Seguiu al @GuardianProject al GitHub'
    github_text: 'Seguir al @GuardianProject'
legal:
  title: 'Orbot | Legal'
  description: 'Orbot - Pàgina legal.'
  title_section: 'Legal'
  legal_items:
  - text: |
      L'**Orbot** és propietat de © 2009-2022 Nathan Freitas i el Guardian Project. Tots els drets reservats. **«Orbot»** i el logotip de l'Orbot són marques comercials de Nathan Freitas. L'Orbot es distribueix sota les condicions de la [llicència 3-clause BSD](https://opensource.org/licenses/BSD-3-Clause)
  - text: 'Aquest programari utilitza criptografia robusta i podria caure sota restriccions
      d''exportació/importació i/o ús en algunes parts del món. ABANS de fer servir
      cap programa de xifratge, comproveu les lleis, regulacions i polítiques del
      vostre país en relació amb la importació, possessió o ús, i a la reexportació
      de programari de xifratge, per a comprovar que es permet. Vegeu [https://www.wassenaar.org](https://www.wassenaar.org)
      per a més informació.'
  - text: 'Si sou un desenvolupador i esteu planejant redistribuir aquesta aplicació
      en forma de codi font o de binari, llegiu el [fitxer LICENSE](https://github.com/guardianproject/orbot/blob/master/LICENSE)
      amb els detalls complets i les obligacions legals.'
navigation:
  navigation_links:
  - title: 'Quant a'
    url: '/about'
  - title: 'Privadesa'
    url: '/privacy-policy'
  - title: 'Baixada'
    url: '/download'
  - title: 'Codi'
    url: '/code'
  - title: 'PMF'
    url: '/faqs'
  - title: 'Legal'
    url: '/legal'
  - title: 'Donació'
    url: '/donate'
privacy:
  title: 'Orbot | Política de privadesa'
  description: 'Orbot - Pàgina de la política de privadesa.'
  title_section: 'Política de privadesa'
  privacy_items:
  - text: "L'Orbot no recopila directament cap data ni activitat de l'usuari. L'Orbot
      no utilitza cap analítica de tercers a banda del que s'incorpora i rastreja
      a Google Play."
  - text: "Podeu obtenir més informació sobre com protegeix el Tor la vostra privadesa
      llegint les [PMF del Tor](https://2019.www.torproject.org/docs/faq.html.en)"
  - text: "Podeu obtenir més informació sobre la visió del Guardian Project quant
      a la privadesa a través de la pàgina de les [Polítiques d'ús i protecció de
      dades](https://guardianproject.info/2016/05/04/data-usage-and-protection-policies/)."
