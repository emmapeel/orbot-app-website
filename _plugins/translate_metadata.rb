# frozen_string_literal: true

Jekyll::Hooks.register :site, :post_read, priority: :low do |site|
  locale = site.config['locale']

  data = site.data.dig(locale, 'site')

  if data
    site.config['title'] = data['title'] if data['title']
    site.config['description'] = data['description'] if data['description']
  end

  site.pages.each do |page|
    base = page.basename
    base = 'home' if base == 'index'
    data = site.data.dig(site.config['locale'], base)

    page.data['image'] ||= { 'path' => site.config['logo'] }

    unless data
      Jekyll.logger.warn 'Metadata:', "Can't find metadata for #{page.relative_path}"
      next
    end

    page.data['title'] = data['title'] if data['title']
    page.data['description'] = data['description'] if data['description']
  end
end
