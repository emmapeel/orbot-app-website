# frozen_string_literal: true

# We want the inverse of Jekyll::Utils.deep_merge_hashes:
#
# 1. Old values are kept unless nil or empty
# 2. Arrays are merged too
def ultra_deep_merge_hashes(main_hash, defaults_hash)
  main_hash.merge(defaults_hash) do |key, old_value, new_value|
    case old_value
    when Hash
      ultra_deep_merge_hashes(old_value, new_value)
    when Array
      old_value.each_with_index.map do |ov, i|
        case ov
        when Hash
          ultra_deep_merge_hashes(ov, new_value[i])
        else
          old_value.nil? ? new_value : old_value
        end
      end
    when String
      old_value.strip.empty? ? new_value : old_value
    else
      old_value || new_value
    end
  end
end

# Loads locales from files instead of relying on someone enabling it on
# _config.yml
#
# English is always the default locale and goes on top of the list.
Jekyll::Hooks.register :site, :after_init, priority: :high do |site|
  Jekyll.logger.info 'Enable locales:', 'Loading locales'

  site.config['locales'] =
    Dir.glob(['_data/??.yml', '_data/??_*.yml']).map do |file|
      File.basename(file, '.yml')
    end.sort_by do |locale|
      next '0' if locale == 'en'

      locale
    end
end

# Sets default values for incomplete translations
Jekyll::Hooks.register :site, :post_read, priority: :high do |site|
  Jekyll.logger.info 'Enable locales:', 'Setting default locale for missing strings'

  site.locales.each do |locale|
    next if locale == site.default_locale

    Jekyll.logger.debug 'Enable locales:', "Setting default locale for missing strings on #{locale}"


    site.data[locale] = ultra_deep_merge_hashes(site.data[locale], site.data[site.default_locale])

    # If locale isn't translated, the name will be listed wrongly on the
    # language selector.
    locale_name = site.data[locale]['home']['locale']
    locale_name_en = site.data.dig('en', 'home', 'locale')

    if locale_name == locale_name_en
      site.data[locale]['home']['locale'] = Jekyll::Utils.titleize_slug(locale.tr('_', ' '))
    end
  end
end
